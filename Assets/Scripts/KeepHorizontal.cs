﻿using UnityEngine;

public class KeepHorizontal : MonoBehaviour
{
    private Quaternion rotation;

    private void Start() => rotation = transform.rotation;

    private void Update() => transform.rotation = rotation;
}
