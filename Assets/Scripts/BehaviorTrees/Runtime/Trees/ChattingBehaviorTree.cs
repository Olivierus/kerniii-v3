﻿using UnityEngine;
using CleverCrow.Fluid.BTs.Trees;

[System.Serializable]
public class ChattingBehaviorTree : BaseBehaviorTree
{
    [Header("Dialogue")]
    [SerializeField] public DialogueObject dialogue = default;

    [Header("Trigger options")]
    [SerializeField] public Transform target = default;

    [SerializeField] public GameObject bubbleRoot = default;
    [SerializeField] public float triggerRange = 2f;

    public override BehaviorTreeBuilder GetBuilder(GameObject gameObject)
    {
        return new BehaviorTreeBuilder(gameObject)
            .Inverter()

                .Selector()

                    .Inverter()

                        .Sequence()

                            .DisableGameObject(bubbleRoot)
                            .TargetInRange(triggerRange, target)
                            .StopMoving()

                        .End()

                    .End()

                    .Sequence()

                        .EnableGameObject(bubbleRoot)

                        .WaitForInput(KeyCode.E)

                        .ShowDialogue(dialogue)

                    .End()

                .End()

            .End();
    }
}
