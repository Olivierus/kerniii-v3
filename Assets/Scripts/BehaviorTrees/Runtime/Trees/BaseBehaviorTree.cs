﻿using UnityEngine;
using CleverCrow.Fluid.BTs.Trees;
using System;

[System.Serializable]
public abstract class BaseBehaviorTree
{
    [NonSerialized] public BehaviorTree behaviorTree = default;

    public virtual void Initialize(GameObject gameObject) => behaviorTree = GetBuilder(gameObject).Build();

    public abstract BehaviorTreeBuilder GetBuilder(GameObject gameObject);
}
