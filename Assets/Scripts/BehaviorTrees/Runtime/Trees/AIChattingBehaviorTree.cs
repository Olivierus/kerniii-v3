﻿using UnityEngine;
using CleverCrow.Fluid.BTs.Trees;
using CleverCrow.Fluid.BTs.Tasks;

[System.Serializable]
public class AIChattingBehaviorTree : BaseBehaviorTree
{

    [SerializeField] public GameObject bubbleAnimation = default;
    [SerializeField] public float triggerRange = 2f;
    [SerializeField] private Vector2Int talkTime = new Vector2Int(2, 4);

    public bool isDone = false;

    public override BehaviorTreeBuilder GetBuilder(GameObject gameObject)
    {
        return new BehaviorTreeBuilder(gameObject)
            .Sequence()

                .EnableGameObject(bubbleAnimation)

                .StopMoving()

                .WaitTime(Random.Range(talkTime.x, talkTime.y))

                .Do("Set is done", () =>
                 {
                     isDone = true;
                     return TaskStatus.Success;
                 })

                .DisableGameObject(bubbleAnimation)

            .End();
    }

}
