using UnityEngine;
using CleverCrow.Fluid.BTs.Trees;

[System.Serializable]
public class WanderingAndStoppingBehaviorTree : BaseBehaviorTree
{
    [Header("Options")]
    [SerializeField] private Transform target = default;
    [SerializeField] public float triggerRange = 5f;

    [SerializeField] public WanderingBehaviorTree wandering = default;

    public override BehaviorTreeBuilder GetBuilder(GameObject gameObject)
    {
        BehaviorTreeBuilder wander = wandering.GetBuilder(gameObject);

        return new BehaviorTreeBuilder(gameObject)
            .Selector()

                .Sequence()

                    .TargetInRange(triggerRange, target)
                    .StopMoving()

                .End()

                .Splice(wander.Build())

            .End();
    }
}
