﻿using UnityEngine;
using CleverCrow.Fluid.BTs.Trees;
using CleverCrow.Fluid.BTs.Tasks;

[System.Serializable]
public class WanderingBehaviorTree : BaseBehaviorTree
{
    [Header("Options")]
    [SerializeField] public Vector2 walkRadius = new Vector2(1, 2);
    [SerializeField] private Vector2 waitTimeRange = new Vector2(0, 2);

    private float endTime = 0;
    [HideInInspector] public bool isDone = false;

    public override BehaviorTreeBuilder GetBuilder(GameObject gameObject)
    {
        return new BehaviorTreeBuilder(gameObject)
            .Selector()

                .Sequence()

                    .IsAtEndOfPath()

                    .Do("StartTimer", () =>
                    {
                        if (endTime != 0)
                            return TaskStatus.Success;

                        float randomTime = Random.Range(waitTimeRange.x, waitTimeRange.y);
                        endTime = Time.realtimeSinceStartup + randomTime;

                        return TaskStatus.Success;
                    })

                    .Inverter()

                        .Sequence()

                            .Do("TimerDone", () =>
                            {
                                TaskStatus taskStatus = Time.realtimeSinceStartup >= endTime ? TaskStatus.Success : TaskStatus.Failure;

                                if (taskStatus == TaskStatus.Success)
                                    endTime = 0;

                                return taskStatus;
                            })

                            .StopMoving()

                            .Do("Set is done", () =>
                             {
                                 isDone = true;
                                 return TaskStatus.Success;
                             })

                        .End()

                    .End()

                .End()

                .Sequence()

                    .Inverter()

                        .IsMoving()

                    .End()

                    .Do("Is done", () => !isDone ? TaskStatus.Success : TaskStatus.Failure)

                    .MoveToPositionInRange(walkRadius.x, walkRadius.y, gameObject.transform.position)

                .End()

            .End();
    }
}
