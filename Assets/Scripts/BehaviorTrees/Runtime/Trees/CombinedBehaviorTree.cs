﻿using CleverCrow.Fluid.BTs.Tasks;
using CleverCrow.Fluid.BTs.Trees;
using UnityEngine;

#pragma warning disable IDE0044

[System.Serializable]
public class CombinedBehaviorTree : BaseBehaviorTree
{
    [SerializeField] public ChattingBehaviorTree chatting = default;
    [SerializeField] private AIChattingBehaviorTree aiChatting = default;
    [SerializeField] public WanderingBehaviorTree wandering = default;
    [SerializeField] private HousingBehaviorTree housing = default;

    [SerializeField] private Vector2Int changeOfGoingHome = new Vector2Int(1, 10);

    private AIState state = AIState.done;
    private AIState lastState = AIState.done;

    public override BehaviorTreeBuilder GetBuilder(GameObject gameObject)
    {
        BehaviorTreeBuilder chat = chatting.GetBuilder(gameObject);
        BehaviorTreeBuilder AIChat = aiChatting.GetBuilder(gameObject);
        BehaviorTreeBuilder wander = wandering.GetBuilder(gameObject);
        BehaviorTreeBuilder house = housing.GetBuilder(gameObject);

        return new BehaviorTreeBuilder(gameObject)
            .Selector()

                .Splice(chat.Build())

                .Sequence()

                    .Do("Is laststate ia chatting", () => lastState != AIState.aiChatting ? TaskStatus.Success : TaskStatus.Failure)

                    .AIInRange(aiChatting.triggerRange, gameObject.transform)

                    // Set housing state
                    .Do("Set ai chatting state", () =>
                    {
                        state = AIState.aiChatting;
                        lastState = AIState.aiChatting;
                        return TaskStatus.Success;
                    })

                .End()

                .Sequence()

                    // Check if done
                    .Do("Check if state is done", () => state == AIState.done ? TaskStatus.Success : TaskStatus.Failure)

                    .Do("Check if not ai chatting", () => state != AIState.aiChatting ? TaskStatus.Success : TaskStatus.Failure)

                    .Do("Reset is done", () =>
                    {
                        wandering.isDone = false;
                        housing.isDone = false;
                        aiChatting.isDone = false;

                        return TaskStatus.Success;
                    })

                    .Selector()

                        .Sequence()

                            .RandomChance(changeOfGoingHome.x, changeOfGoingHome.y)

                            // Set housing state
                            .Do("Set housing state", () =>
                            {
                                state = AIState.housing;
                                lastState = AIState.aiChatting;

                                return TaskStatus.Success;
                            })

                        .End()

                        // Set wandering state
                        .Do("Set wandering state", () =>
                        {
                            state = AIState.wandering;
                            return TaskStatus.Success;
                        })

                    .End()

                .End()

                // Checking state
                .Selector()

                    // Check if aichatting state
                    .Sequence()

                        .Do("Is state ai chatting", () => state == AIState.aiChatting ? TaskStatus.Success : TaskStatus.Failure)

                        .Selector()

                            .Sequence()

                                .Do("ai chatting is done", () => aiChatting.isDone ? TaskStatus.Success : TaskStatus.Failure)

                                // Set wandering state
                                .Do("Set done state", () =>
                                {
                                    state = AIState.done;
                                    return TaskStatus.Success;
                                })

                            .End()

                            // Is wandering state
                            .Splice(AIChat.Build())

                        .End()

                    .End()

                    // Check if housing state
                    .Sequence()

                        .Do("Is state housing", () => state == AIState.housing ? TaskStatus.Success : TaskStatus.Failure)

                        .Selector()

                            .Sequence()

                                .Do("Housing is done", () => housing.isDone ? TaskStatus.Success : TaskStatus.Failure)

                                // Set wandering state
                                .Do("Set done state", () =>
                                {
                                    state = AIState.done;
                                    lastState = AIState.done;

                                    return TaskStatus.Success;
                                })

                            .End()

                            // Is wandering state
                            .Splice(house.Build())

                        .End()

                    .End()

                    // Check if wandering state
                    .Sequence()

                        .Do("Is state wandering", () => state == AIState.wandering ? TaskStatus.Success : TaskStatus.Failure)

                        .Selector()

                            .Sequence()

                                .Do("Wandering is done", () => wandering.isDone ? TaskStatus.Success : TaskStatus.Failure)

                                // Set wandering state
                                .Do("Set done state", () =>
                                {
                                    state = AIState.done;
                                    lastState = AIState.done;

                                    return TaskStatus.Success;
                                })

                            .End()

                            // Is wandering state
                            .Splice(wander.Build())

                        .End()

                    .End()

                .End()

            .End();
    }
}

public enum AIState
{
    wandering,
    housing,
    aiChatting,
    done
}
