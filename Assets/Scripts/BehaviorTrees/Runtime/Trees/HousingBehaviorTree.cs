﻿using UnityEngine;
using CleverCrow.Fluid.BTs.Trees;
using CleverCrow.Fluid.BTs.Tasks;

[System.Serializable]
public class HousingBehaviorTree : BaseBehaviorTree
{
    [Header("House")]
    [SerializeField] private Transform door = default;

    [Header("InHouse")]
    [SerializeField] private Vector2 waitTimeRange = new Vector2(0, 2);
    [SerializeField] private Renderer aiRenderer = default;
    [SerializeField] private Collider aiCollider = default;

    [HideInInspector] public bool isDone = false;

    public override BehaviorTreeBuilder GetBuilder(GameObject gameObject)
    {
        return new BehaviorTreeBuilder(gameObject)
            .Selector()

                .Sequence()

                    .IsAtEndOfPath("Is at door house")

                    .Do("Disable Renderer and Collider", () =>
                    {
                        aiRenderer.enabled = false;
                        aiCollider.enabled = false;
                        return TaskStatus.Success;
                    })

                    .WaitTime(Random.Range(waitTimeRange.x, waitTimeRange.y))

                    .Do("Enable Renderer and Collider", () =>
                    {
                        aiRenderer.enabled = true;
                        aiCollider.enabled = true;
                        return TaskStatus.Success;
                    })

                    .Do("Set is done", () =>
                     {
                         isDone = true;
                         return TaskStatus.Success;
                     })

                .End()

                .Sequence()

                    .Inverter()

                        .IsMoving()

                    .End()

                    .Do("Is done", () => !isDone ? TaskStatus.Success : TaskStatus.Failure)

                    .MoveToPosition(door)

                .End()

            .End();
    }
}
