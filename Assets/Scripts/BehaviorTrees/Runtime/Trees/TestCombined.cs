﻿using CleverCrow.Fluid.BTs.Tasks;
using CleverCrow.Fluid.BTs.Trees;
using UnityEngine;

public class TestCombined : MonoBehaviour
{
    [SerializeField] private BehaviorTree tree;

    private void Awake()
    {
        BehaviorTreeBuilder injectTree = new BehaviorTreeBuilder(gameObject)
            .Sequence()
                .Do("Custom Action", () =>
                {
                    return TaskStatus.Success;
                })
            .End();

        tree = new BehaviorTreeBuilder(gameObject)
            .Sequence()
                .Splice(injectTree.Build())
                .Do("Custom Action", () =>
                {
                    return TaskStatus.Success;
                })
            .End()
            .Build();
    }

    private void Update() => tree.Tick();
}
