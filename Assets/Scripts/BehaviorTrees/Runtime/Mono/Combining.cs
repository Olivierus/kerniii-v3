﻿using CleverCrow.Fluid.BTs.Trees;
using UnityEngine;

#pragma warning disable IDE0051

public class Combining : MonoBehaviour
{
    [SerializeField] private BehaviorTree behaviorTree = default;

    [SerializeField] private CombinedBehaviorTree combinedBehaviorTree = default;

    private void Awake()
    {
        combinedBehaviorTree.Initialize(gameObject);
        behaviorTree = combinedBehaviorTree.behaviorTree;
    }

    private void Update() => combinedBehaviorTree.behaviorTree.Tick();

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, combinedBehaviorTree.wandering.walkRadius.x);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, combinedBehaviorTree.wandering.walkRadius.y);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, combinedBehaviorTree.chatting.triggerRange);
    }
}
