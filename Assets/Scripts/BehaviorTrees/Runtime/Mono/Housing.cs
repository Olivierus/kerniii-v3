﻿using CleverCrow.Fluid.BTs.Trees;
using UnityEngine;

#pragma warning disable IDE0051

public class Housing : MonoBehaviour
{
    [SerializeField] private BehaviorTree behaviorTree = default;

    [SerializeField] private HousingBehaviorTree housingBehaviorTree = default;

    private void Awake()
    {
        housingBehaviorTree.Initialize(gameObject);
        behaviorTree = housingBehaviorTree.behaviorTree;
    }

    private void Update() => housingBehaviorTree.behaviorTree.Tick();
}
