﻿using CleverCrow.Fluid.BTs.Trees;
using UnityEngine;

#pragma warning disable IDE0051

public class WanderingAndStopping : MonoBehaviour
{
    [SerializeField] private BehaviorTree behaviorTree = default;

    [SerializeField] private WanderingAndStoppingBehaviorTree wanderingAndStoppingBehaviorTree = default;

    private void Awake()
    {
        wanderingAndStoppingBehaviorTree.Initialize(gameObject);
        behaviorTree = wanderingAndStoppingBehaviorTree.behaviorTree;
    }

    private void Update() => wanderingAndStoppingBehaviorTree.behaviorTree.Tick();

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, wanderingAndStoppingBehaviorTree.wandering.walkRadius.x);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, wanderingAndStoppingBehaviorTree.wandering.walkRadius.y);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, wanderingAndStoppingBehaviorTree.triggerRange);
    }
}
