﻿using CleverCrow.Fluid.BTs.Trees;
using UnityEngine;

#pragma warning disable IDE0051

public class Wandering : MonoBehaviour
{
    [SerializeField] private BehaviorTree behaviorTree = default;

    [SerializeField] private WanderingBehaviorTree wanderingBehaviorTree = default;

    private void Awake()
    {
        wanderingBehaviorTree.Initialize(gameObject);
        behaviorTree = wanderingBehaviorTree.behaviorTree;
    }

    private void Update() => wanderingBehaviorTree.behaviorTree.Tick();

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, wanderingBehaviorTree.walkRadius.x);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, wanderingBehaviorTree.walkRadius.y);
    }
}
