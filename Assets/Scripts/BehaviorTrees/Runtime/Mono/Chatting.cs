﻿using UnityEngine;
using CleverCrow.Fluid.BTs.Trees;

#pragma warning disable IDE0051

public class Chatting : MonoBehaviour
{
    [SerializeField] private BehaviorTree behaviorTree = default;
    [SerializeField] private ChattingBehaviorTree chattingBehaviorTree = default;

    private void Awake()
    {
        chattingBehaviorTree.Initialize(gameObject);
        behaviorTree = chattingBehaviorTree.behaviorTree;
    }

    private void Update() => chattingBehaviorTree.behaviorTree.Tick();

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, chattingBehaviorTree.triggerRange);
    }
}
