using UnityEngine;
using CleverCrow.Fluid.BTs.Trees;
using System.Collections;
using DutchSkull.Gizmos;

#pragma warning disable IDE0051

public class AIChatting : MonoBehaviour
{
    [SerializeField] private BehaviorTree behaviorTree = default;
    [SerializeField] private AIChattingBehaviorTree aiChattingBehaviorTree = default;

    private IEnumerator Start()
    {
        yield return AIManager.WaitForLoaded();

        aiChattingBehaviorTree.Initialize(gameObject);
        behaviorTree = aiChattingBehaviorTree.behaviorTree;
    }

    private void Update()
    {
        if (aiChattingBehaviorTree.behaviorTree == null)
            return;

        aiChattingBehaviorTree.behaviorTree.Tick();
    }

    private void OnDrawGizmos()
    {
        if (!Application.isPlaying || !enabled)
            return;

        Vector3 closestAI = AIManager.Instance.Closest(transform).position;

        GizmosExtension.DrawLineVector3(transform.position, closestAI);
    }
}
