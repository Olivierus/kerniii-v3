﻿using CleverCrow.Fluid.BTs.Tasks;
using CleverCrow.Fluid.BTs.Tasks.Actions;
using UnityEngine;

public class ShowDialogue : ActionBase
{
    public DialogueObject dialogue;

    protected override void OnStart() => DialogueManager.StartDialogue(dialogue);

    protected override TaskStatus OnUpdate()
    {
        if (DialogueManager.isDone)
            return TaskStatus.Success;
        else
            return TaskStatus.Continue;
    }

    private DialogueManager DialogueManager => DialogueManager.Instance;
}
