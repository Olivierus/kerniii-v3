using CleverCrow.Fluid.BTs.Tasks;

public class MoveToBase : MoveBase
{
    protected override void OnStart() => hasFoundAPath = pathFinder.SetDestination(destination);

    protected override TaskStatus OnUpdate() => hasFoundAPath ? TaskStatus.Success : TaskStatus.Failure;

    protected override void OnExit()
    {
        destination = default;
        hasFoundAPath = false;
    }
}
