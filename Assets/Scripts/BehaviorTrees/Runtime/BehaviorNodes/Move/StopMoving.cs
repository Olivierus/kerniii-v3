using CleverCrow.Fluid.BTs.Tasks;

public class StopMoving : MoveToBase
{
    protected override TaskStatus OnUpdate()
    {
        pathFinder.Stop();
        return TaskStatus.Success;
    }
}
