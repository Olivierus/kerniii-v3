﻿using UnityEngine;
using CleverCrow.Fluid.BTs.Tasks;

public class TargetInRange : ConditionBase
{
    public float triggerRange;
    public Transform targetTransform;

    public override string IconPath => base.IconPath;

    public override float IconPadding => base.IconPadding;

    protected override bool OnUpdate()
    {
        float distanceToTarget = Vector3.Distance(Owner.transform.position, targetTransform.position);

        return distanceToTarget <= triggerRange;
    }
}
