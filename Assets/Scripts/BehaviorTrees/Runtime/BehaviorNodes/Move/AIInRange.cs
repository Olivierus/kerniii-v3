using UnityEngine;
using CleverCrow.Fluid.BTs.Tasks;

public class AIInRange : ConditionBase
{
    public float triggerRange;
    public Transform transform;

    public override string IconPath => base.IconPath;

    public override float IconPadding => base.IconPadding;

    protected override bool OnUpdate()
    {
        float distanceToTarget = Vector3.Distance(Owner.transform.position, AIManager.Closest(transform).position);
        return distanceToTarget <= triggerRange;
    }

    private AIManager AIManager => AIManager.Instance;
}
