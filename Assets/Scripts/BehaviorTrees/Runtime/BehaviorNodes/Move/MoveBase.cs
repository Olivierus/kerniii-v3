using CleverCrow.Fluid.BTs.Tasks;
using CleverCrow.Fluid.BTs.Tasks.Actions;
using UnityEngine;

public abstract class MoveBase : ActionBase
{
    protected AstarAgent pathFinder;

    protected Vector3 destination = default;

    protected bool hasFoundAPath = false;

    protected override void OnInit() => pathFinder = Owner.GetComponent<AstarAgent>();

    protected override abstract TaskStatus OnUpdate();
}
