using CleverCrow.Fluid.BTs.Tasks;

public class IsAtEndOfPath : ConditionBase
{
    protected AstarAgent pathFinder;

    protected override void OnInit() => pathFinder = Owner.GetComponent<AstarAgent>();

    protected override bool OnUpdate() => pathFinder.RemainingDistance <= 1f;
}

public class IsMoving : ConditionBase
{
    protected AstarAgent pathFinder;

    protected override void OnInit() => pathFinder = Owner.GetComponent<AstarAgent>();

    protected override bool OnUpdate() => pathFinder.isMoving;
}
