using UnityEngine;

public class MoveToPosition : MoveToBase
{
    public Transform target;

    protected override void OnStart()
    {
        destination = new Vector2
        {
            x = target.position.x,
            y = target.position.z
        };

        Debug.Log($"{nameof(MoveToPosition)}: From {target.position} to {destination}");

        base.OnStart();
    }
}
