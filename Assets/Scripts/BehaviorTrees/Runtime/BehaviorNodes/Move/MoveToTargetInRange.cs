﻿using UnityEngine;

public class MoveToPositionInRange : MoveToBase
{
    public float minimunRange = 1;
    public float maximumRange = 2;
    public Vector2 position = default;

    protected override void OnStart()
    {
        while (!hasFoundAPath)
        {
            Random.InitState(System.DateTime.Now.Millisecond);
            Vector2 randomVector = Random.insideUnitCircle * maximumRange;

            Vector2 nextRandomPosition = randomVector + position;

            if (Vector2.Distance(nextRandomPosition, position) < minimunRange)
                continue;

            destination = nextRandomPosition;

            base.OnStart();
        }
    }
}
