﻿using UnityEngine;
using CleverCrow.Fluid.BTs.Tasks;
using CleverCrow.Fluid.BTs.Tasks.Actions;

public class WaitForInput : ConditionBase
{
    public KeyCode key;

    protected override bool OnUpdate() => Input.GetKeyDown(key);
}
