using CleverCrow.Fluid.BTs.Tasks;
using CleverCrow.Fluid.BTs.Tasks.Actions;
using UnityEngine;

public class EnableComponent : ActionBase
{
    public Behaviour component;

    protected override TaskStatus OnUpdate()
    {
        component.enabled = true;
        return TaskStatus.Success;
    }
}
