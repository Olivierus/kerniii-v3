using CleverCrow.Fluid.BTs.Tasks;
using CleverCrow.Fluid.BTs.Tasks.Actions;
using UnityEngine;

public class EnableGameObject : ActionBase
{
    public GameObject gameObject;

    protected override TaskStatus OnUpdate()
    {
        gameObject.SetActive(true);
        return TaskStatus.Success;
    }
}
