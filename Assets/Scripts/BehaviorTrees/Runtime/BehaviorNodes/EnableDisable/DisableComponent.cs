using CleverCrow.Fluid.BTs.Tasks;
using CleverCrow.Fluid.BTs.Tasks.Actions;
using UnityEngine;

public class DisableComponent : ActionBase
{
    public Behaviour component;

    protected override TaskStatus OnUpdate()
    {
        component.enabled = false;
        return TaskStatus.Success;
    }
}
