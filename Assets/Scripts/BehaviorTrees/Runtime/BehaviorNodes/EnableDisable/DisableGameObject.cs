using CleverCrow.Fluid.BTs.Tasks;
using CleverCrow.Fluid.BTs.Tasks.Actions;
using UnityEngine;

public class DisableGameObject : ActionBase
{
    public GameObject gameObject;

    protected override TaskStatus OnUpdate()
    {
        gameObject.SetActive(false);
        return TaskStatus.Success;
    }
}
