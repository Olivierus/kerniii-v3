﻿using CleverCrow.Fluid.BTs.Trees;
using UnityEngine;

public static class BehaviorTreeBuilderExtensions
{
    #region Moving

    public static BehaviorTreeBuilder MoveToPositionInRange(this BehaviorTreeBuilder builder, float minimumRange, float maximumRange, Vector3 position, string name = nameof(MoveToPositionInRange)) =>
        builder.AddNode(new MoveToPositionInRange { minimunRange = minimumRange, maximumRange = maximumRange, position = position, Name = name });

    public static BehaviorTreeBuilder MoveToPosition(this BehaviorTreeBuilder builder, Transform target, string name = nameof(MoveToPosition)) =>
        builder.AddNode(new MoveToPosition { target = target, Name = name });

    public static BehaviorTreeBuilder StopMoving(this BehaviorTreeBuilder builder, string name = nameof(StopMoving)) =>
        builder.AddNode(new StopMoving { Name = name });

    public static BehaviorTreeBuilder IsAtEndOfPath(this BehaviorTreeBuilder builder, string name = nameof(IsAtEndOfPath)) =>
        builder.AddNode(new IsAtEndOfPath { Name = name });

    public static BehaviorTreeBuilder IsMoving(this BehaviorTreeBuilder builder, string name = nameof(IsMoving)) =>
        builder.AddNode(new IsMoving { Name = name });

    #endregion

    #region Waiting

    public static BehaviorTreeBuilder WaitForInput(this BehaviorTreeBuilder builder, KeyCode key, string name = nameof(WaitForInput)) =>
        builder.AddNode(new WaitForInput { key = key, Name = name });

    #endregion

    public static BehaviorTreeBuilder TargetInRange(this BehaviorTreeBuilder builder, float triggerRange, Transform targetTransform, string name = nameof(TargetInRange)) =>
        builder.AddNode(new TargetInRange { targetTransform = targetTransform, triggerRange = triggerRange, Name = name });

    public static BehaviorTreeBuilder AIInRange(this BehaviorTreeBuilder builder, float triggerRange, Transform transform, string name = nameof(AIInRange)) =>
        builder.AddNode(new AIInRange { transform = transform, triggerRange = triggerRange, Name = name });

    public static BehaviorTreeBuilder EnableComponent(this BehaviorTreeBuilder builder, Behaviour component, string name = nameof(EnableComponent)) =>
        builder.AddNode(new EnableComponent { component = component, Name = name });

    public static BehaviorTreeBuilder DisableComponent(this BehaviorTreeBuilder builder, Behaviour component, string name = nameof(DisableComponent)) =>
        builder.AddNode(new DisableComponent { component = component, Name = name });

    public static BehaviorTreeBuilder EnableGameObject(this BehaviorTreeBuilder builder, GameObject gameObject, string name = nameof(EnableComponent)) =>
        builder.AddNode(new EnableGameObject { gameObject = gameObject, Name = name });

    public static BehaviorTreeBuilder DisableGameObject(this BehaviorTreeBuilder builder, GameObject gameObject, string name = nameof(DisableComponent)) =>
        builder.AddNode(new DisableGameObject { gameObject = gameObject, Name = name });

    public static BehaviorTreeBuilder ShowDialogue(this BehaviorTreeBuilder builder, DialogueObject dialogue, string name = nameof(ShowDialogue)) =>
        builder.AddNode(new ShowDialogue { dialogue = dialogue, Name = name });
}