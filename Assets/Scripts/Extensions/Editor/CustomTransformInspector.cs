﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

#pragma warning disable IDE0051

[CustomEditor(typeof(Transform), true)]
[CanEditMultipleObjects]
public class CustomTransformInspector : Editor
{
    private const string LOCAL_LABEL = "Local";
    private const string WORLD_LABEL = "World";
    private const string DEVIDER = " ";
    private const string SPACE_LABEL = "Space";

    private Editor defaultEditor;
    private Transform transform;

    private void OnEnable()
    {
        defaultEditor = CreateEditor(targets, Type.GetType("UnityEditor.TransformInspector, UnityEditor"));

        transform = target as Transform;
    }

    private void OnDisable()
    {
        MethodInfo disableMethod = defaultEditor.GetType().GetMethod("OnDisable", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

        if (disableMethod != null)
            disableMethod.Invoke(defaultEditor, null);

        DestroyImmediate(defaultEditor);
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField($"{LOCAL_LABEL}{DEVIDER}{SPACE_LABEL}", EditorStyles.boldLabel);

        EditorGUI.indentLevel++;

        defaultEditor.OnInspectorGUI();

        EditorGUI.indentLevel--;

        EditorGUILayout.Space();

        EditorGUILayout.LabelField($"{WORLD_LABEL}{DEVIDER}{SPACE_LABEL}", EditorStyles.boldLabel);

        EditorGUI.indentLevel++;

        GUI.enabled = false;
        Vector3 localPosition = transform.localPosition;
        transform.localPosition = transform.position;

        Quaternion localRotation = transform.localRotation;
        transform.localRotation = transform.rotation;

        Vector3 localScale = transform.localScale;
        transform.localScale = transform.lossyScale;

        defaultEditor.OnInspectorGUI();

        EditorGUI.indentLevel--;

        transform.localPosition = localPosition;
        transform.localRotation = localRotation;
        transform.localScale = localScale;
        GUI.enabled = true;
    }
}