using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class ScriptableObjectBuilder<T> where T : ScriptableObject
{
    public T ScriptableObject { get; private set; }

    public ScriptableObjectBuilder(string folderPath, string assetName) => InitializeObject(folderPath, assetName);

    public T InitializeObject(string folderPath, string assetName)
    {
        ScriptableObject = LoadObject(assetName);

#if UNITY_EDITOR

        if (ScriptableObject == null)
            ScriptableObject = CreateObject(folderPath, assetName);

        SaveObject(ScriptableObject);

#endif

        return ScriptableObject;
    }

    private T LoadObject(string assetName)
    {
        if (assetName.Contains(".Asset"))
            assetName.Remove(assetName.IndexOf(".Asset"), ".Asset".Length);

        T mapData = Resources.Load<T>(assetName);

        if (mapData == null)
            return null;

        return mapData;
    }

#if UNITY_EDITOR

    private T CreateObject(string folderPath, string assetName)
    {
        T scriptableObject = UnityEngine.ScriptableObject.CreateInstance<T>();

        folderPath += folderPath.Last() == '/' ? "" : "/";

        Directory.CreateDirectory(folderPath);

        AssetDatabase.Refresh();

        assetName += assetName.Contains(".Asset") ? "" : ".Asset";

        AssetDatabase.CreateAsset(scriptableObject, $"{folderPath}/{assetName}");

        return scriptableObject;
    }

    private void SaveObject(T scriptableObject)
    {
        EditorUtility.SetDirty(scriptableObject);

        AssetDatabase.SaveAssets();

        AssetDatabase.Refresh();
    }

#endif
}