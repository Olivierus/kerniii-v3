﻿using UnityEngine;

namespace DutchSkull.Utilities
{
    public static class Vector3Utilities
    {
        public static Vector3 Random(Vector3 bottomLeft, Vector3 topRight) => new Vector3()
        {
            x = UnityEngine.Random.Range(bottomLeft.x, topRight.x),
            y = UnityEngine.Random.Range(bottomLeft.y, topRight.y),
            z = UnityEngine.Random.Range(bottomLeft.z, topRight.z)
        };

        public static Vector2 GetXZ(this Vector3 vector) => new Vector2()
        {
            x = vector.x,
            y = vector.z
        };
    }
}