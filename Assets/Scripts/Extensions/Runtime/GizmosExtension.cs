﻿namespace DutchSkull.Gizmos
{
    using System.Collections.Generic;
    using UnityEngine;

    public class GizmosExtension
    {
        public static void DrawBeginAndEndPoint(Vector2 start, Vector2 goal)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(new Vector3(start.x, 0, start.y), .5f);

            Gizmos.color = Color.red;
            Gizmos.DrawSphere(new Vector3(goal.x, 0, goal.y), .5f);

            Gizmos.color = Color.green;
        }

        public static void DrawPath(List<Vector2> path)
        {
            Vector2 lastPathPoint = path[0];

            for (int j = 1; j < path.Count; j++)
            {
                DrawLineVector2(lastPathPoint, path[j]);

                lastPathPoint = path[j];
            }
        }

        public static void DrawLineVector2(Vector2 start, Vector2 goal)
        {
            Vector3 startPoint = new Vector3(start.x, 0, start.y);
            Vector3 goalPoint = new Vector3(goal.x, 0, goal.y);

            DrawLineVector3(startPoint, goalPoint);
        }

        public static void DrawLineVector3(Vector3 start, Vector3 goal) => Gizmos.DrawLine(start, goal);
    }
}