﻿using UnityEngine;


namespace DutchSkull.Utilities
{
    public static class Vector2Utilities
    {
        public static Vector2 Random(Vector2 bottomLeft, Vector2 topRight) => new Vector2()
        {
            x = UnityEngine.Random.Range(bottomLeft.x, topRight.x),
            y = UnityEngine.Random.Range(bottomLeft.y, topRight.y)
        };

        public static Vector2Int ToInt2(this Vector2 vector) => new Vector2Int((int)vector.x, (int)vector.y);

        public static Vector3 ToXZ(this Vector2 vector) => new Vector3(vector.x, 0, vector.y);
    }
}