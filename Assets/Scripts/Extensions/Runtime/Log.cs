﻿using UnityEngine;


public static class Log
{
    public static T Message<T>(object message, T respons)
    {
        Debug.Log(message);
        return respons;
    }

    public static T Message<T>(object message)
    {
        Debug.Log(message);
        return default;
    }

    public static T Warning<T>(object message, T respons, Object context = default)
    {
        Debug.LogWarning(message, context);
        return respons;
    }

    public static T Error<T>(object message, T respons)
    {
        Debug.LogError(message);
        return respons;
    }
}
