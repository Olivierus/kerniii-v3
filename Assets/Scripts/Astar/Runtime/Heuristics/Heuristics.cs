﻿using System;

namespace DutchSkull.Astar
{
    public class Heuristics
    {
        internal static double Diagonal(Node start, Node end) => Math.Max(Math.Abs(start.X - end.X), Math.Abs(start.Y - end.Y));

        internal static double Euclidean(Node start, Node end) => Math.Sqrt((start.X - end.X) * (start.X - end.X) + (start.Y - end.Y) * (start.Y - end.Y));

        internal static double Manhattan(Node start, Node end) => Math.Abs(start.X - end.X) + Math.Abs(start.Y - end.Y);
    }
}