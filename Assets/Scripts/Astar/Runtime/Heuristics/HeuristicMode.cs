﻿namespace DutchSkull.Astar
{
    public enum HeuristicMode
    {
        Diagonal,
        Euclidean,
        Manhattan
    }
}