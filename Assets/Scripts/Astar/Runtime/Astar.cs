﻿using DutchSkull.Singleton;
using DutchSkull.Utilities;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor.SceneManagement;
using UnityEditor;
#endif

namespace DutchSkull.Astar
{
    // The base A star code comes from here. Heavily refactored.
    // https://gist.github.com/stylophone/3448727
    [ExecuteAlways]
    public class Astar : SingleSceneSingleton<Astar>
    {
        private static ScriptableObjectBuilder<AStarMapData> mapData;
        private readonly string currentSceneName;

        public Vector2 Size => mapData.ScriptableObject.map.size;

        protected override void Awake() => SetInstance(this);

        private void Start() => LoadSceneMapData();

        public void OnEnable()
        {
#if UNITY_EDITOR
            EditorApplication.playModeStateChanged += EditorApplication_playModeStateChanged;
            AssemblyReloadEvents.afterAssemblyReload += OnAfterAssemblyReload;
            EditorSceneManager.sceneOpened += EditorSceneManager_sceneOpened;
#endif
            SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        }

        public void OnDisable()
        {
#if UNITY_EDITOR
            EditorApplication.playModeStateChanged -= EditorApplication_playModeStateChanged;
            AssemblyReloadEvents.afterAssemblyReload -= OnAfterAssemblyReload;
            EditorSceneManager.sceneOpened -= EditorSceneManager_sceneOpened;
#endif
            SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
        }

#if UNITY_EDITOR

        private void EditorApplication_playModeStateChanged(PlayModeStateChange playMode)
        {
            if (Application.isPlaying)
                return;
            else
                LoadSceneMapData();
        }

        private void OnAfterAssemblyReload()
        {
            Awake();
            LoadSceneMapData();
        }

        private void EditorSceneManager_sceneOpened(Scene scene, OpenSceneMode mode) => LoadSceneMapData();

#endif

        private void SceneManager_sceneLoaded(Scene scene, LoadSceneMode mode) => LoadSceneMapData();

        private static void LoadSceneMapData() => mapData = new ScriptableObjectBuilder<AStarMapData>(AstarConfig.FolderPath, AstarConfig.AssetName);

        public bool GetPath(Vector2 start, Vector2 goal, out List<Vector2> path, HeuristicMode heuristicMethod = HeuristicMode.Euclidean)
        {
            path = null;

            if (mapData == null)
                return Log.Warning($"{nameof(Astar)}: Map data was not loaded properly.", false, this);

            path = Run(start, goal.ToInt2(), heuristicMethod);

            if (path == null)
                return Log.Warning($"{nameof(Astar)}: Position of AI is out of bounds.", false, this);
            else if (path.Count <= 0)
                return Log.Warning($"{nameof(Astar)}: The goal is outside of the map area. Please select another point.", false, this);

            path.Add(goal);

            return true;
        }

        public List<Vector2> Run(Vector2 start, Vector2 goal, HeuristicMode heuristicMethod)
        {
            if (IsOutOfMapBounds(Vector2Int.FloorToInt(start), Vector2Int.FloorToInt(goal)))
                return null;

            switch (heuristicMethod)
            {
                case HeuristicMode.Euclidean:
                    return RunAStar(mapData.ScriptableObject, start, goal, Heuristics.Euclidean, heuristicMethod);
                case HeuristicMode.Diagonal:
                    return RunAStar(mapData.ScriptableObject, start, goal, Heuristics.Diagonal, heuristicMethod);
                case HeuristicMode.Manhattan:
                    return RunAStar(mapData.ScriptableObject, start, goal, Heuristics.Manhattan, heuristicMethod);
                default:
                    return null;
            }
        }

        private bool IsOutOfMapBounds(Vector2Int start, Vector2Int goal)
        {
            if (start.x <= 0 || start.y <= 0 || goal.x <= 0 || goal.y <= 0)
                return true;
            else if (start.x >= mapData.ScriptableObject.map.size.x ||
                    start.y >= mapData.ScriptableObject.map.size.y ||
                    goal.x >= mapData.ScriptableObject.map.size.x ||
                    goal.y >= mapData.ScriptableObject.map.size.y)
                return true;

            return false;
        }

        public List<Vector2> RunAStar(AStarMapData mapData, Vector2 start, Vector2 goal, Func<Node, Node, double> heuristic, HeuristicMode heuristicMethod)
        {
            List<Vector2> result = new List<Vector2>();

            int[] startIndexes = new int[] { Mathf.RoundToInt(start.x), Mathf.RoundToInt(start.y) };
            int[] endIndexes = new int[] { Mathf.RoundToInt(goal.x), Mathf.RoundToInt(goal.y) };

            if (mapData.map.data.Length <= 0)
            {
                Debug.LogWarning($"{nameof(Astar)}: The map size can't be negative or 0. Please check.");
                return result;
            }

            int columns = mapData.map.size.x;
            int rows = mapData.map.size.y;
            int limit = columns * rows;
            int length = 1;

            List<Node> open = new List<Node> { new Node(startIndexes[0], startIndexes[1]) };

            open[0].FScore = 0;
            open[0].GScore = 0;
            open[0].V = startIndexes[0] + startIndexes[1] * columns;

            Node current;

            List<int> closed = new List<int>();

            double distanceS;
            double distanceE;

            int i;
            int j;

            double max;
            int min;

            Node[] next;
            Node adjacent;

            Node end = new Node(endIndexes[0], endIndexes[1]) { V = endIndexes[0] + endIndexes[1] * columns };

            bool inList;

            do
            {
                max = limit;
                min = 0;

                for (i = 0; i < length; i++)
                {
                    if (open[i].FScore >= max)
                        continue;

                    max = open[i].FScore;
                    min = i;
                }

                current = open[min];
                open.RemoveAt(min);

                if (current.V != end.V)
                {
                    --length;

                    next = current.Successors(mapData, rows, columns, heuristicMethod);

                    for (i = 0, j = next.Length; i < j; ++i)
                    {
                        if (next[i] == null)
                            continue;

                        (adjacent = next[i]).Parent = current;
                        adjacent.FScore = adjacent.GScore = 0;
                        adjacent.V = adjacent.X + adjacent.Y * columns;
                        inList = false;

                        for (int k = 0; k < closed.Count; k++)
                        {
                            int key = closed[k];

                            if (adjacent.V != key)
                                continue;

                            inList = true;
                        }

                        if (!inList)
                        {
                            distanceS = heuristic(adjacent, current);
                            distanceE = heuristic(adjacent, end);

                            adjacent.FScore = (adjacent.GScore = current.GScore + distanceS) + distanceE;
                            open.Add(adjacent);
                            closed.Add(adjacent.V);
                            length++;
                        }
                    }
                }
                else
                {
                    length = 0;

                    do
                        result.Add(current.Position);
                    while
                        ((current = current.Parent) != null);

                    result.Reverse();
                    return result;
                }

            }
            while (length != 0);

            return result;
        }
    }
}
