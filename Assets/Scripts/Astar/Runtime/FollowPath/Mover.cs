﻿using UnityEngine;

public class Mover
{
    private const float DISTANCE_MARGIN = 0.1f;

    public float moveSpeed = 0;

    private readonly Transform transform = default;
    private readonly MoveMethod moveMethod = MoveMethod.Transform;
    private readonly UnityEngine.CharacterController characterController = default;

    public Mover(Transform transform, float moveSpeed, MoveMethod moveMethod = MoveMethod.Transform)
    {
        this.transform = transform;
        this.moveSpeed = moveSpeed;

        this.moveMethod = moveMethod;

        switch (moveMethod)
        {
            case MoveMethod.CharacterController:
                if (transform.TryGetComponent(out characterController))
                    break;

                Debug.LogWarning($"{nameof(FollowPath)}: No character controller found on {transform.name}. Make sure there is one. Set to transform movement fallback.");
                moveMethod = MoveMethod.Transform;
                break;
            default:
                break;
        }
    }


    public void Move(Vector3 direction)
    {
        if (direction == Vector3.zero)
            return;

        switch (moveMethod)
        {
            case MoveMethod.Transform:
                TransformMove(direction);
                break;
            case MoveMethod.CharacterController:
                CharacterControllerMove(direction);
                break;
            default:
                break;
        }
    }

    private void CharacterControllerMove(Vector3 direction)
    {
        Vector3 offset = direction - transform.position;

        if (offset.magnitude <= DISTANCE_MARGIN)
            return;

        offset = offset.normalized * moveSpeed;
        offset *= Time.unscaledDeltaTime;

        characterController.Move(offset);
    }

    private void TransformMove(Vector3 direction)
    {
        float step = moveSpeed * Time.unscaledDeltaTime;

        Vector3 nextPosition = Vector3.MoveTowards(transform.position, direction, step);

        transform.position = nextPosition;
    }
}
