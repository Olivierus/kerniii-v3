using System.Collections.Generic;
using DutchSkull.Utilities;
using UnityEngine;

public class FollowPath
{
    public List<Vector2> Path { get; private set; }

    private const float DISTANCE_MARGIN = 0.1f;

    private readonly Transform transform = default;
    private readonly float rotationSpeed = 0;
    private readonly Mover mover = default;

    private int currentNodeIndex = 0;

    public FollowPath(List<Vector2> path, Transform transform, float moveSpeed = 1, float rotationSpeed = 1, MoveMethod moveMethod = MoveMethod.Transform)
    {
        this.Path = path;
        this.rotationSpeed = rotationSpeed;
        this.transform = transform;

        this.mover = new Mover(transform, moveSpeed, moveMethod);
    }

    public void MoveOnPath()
    {
        if (Path == null)
            return;

        if (currentNodeIndex >= Path.Count)
            return;

        //TODO Make this generic
        Vector3 position = Path[currentNodeIndex].ToXZ();
        position.y = transform.position.y;

        mover.Move(position);

        RotateTowardsTarget(position);

        float distance = Vector3.Distance(position, transform.position);

        //TODO add is add destination
        if (distance > DISTANCE_MARGIN)
            return;

        currentNodeIndex++;
    }

    private void RotateTowardsTarget(Vector3 position)
    {
        Vector3 direction = position - transform.position;

        if (direction == Vector3.zero)
            return;

        direction.y = 0;
        direction = direction.normalized;
        Quaternion rotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
    }
}
