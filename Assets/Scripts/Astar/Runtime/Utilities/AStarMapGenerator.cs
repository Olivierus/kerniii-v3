﻿using UnityEngine;

public static class AStarMapGenerator
{
#if UNITY_EDITOR

    private static readonly RaycastHit[] RESULT = new RaycastHit[1];

    public static OneDArray Generate(Vector2Int size)
    {
        OneDArray map = new OneDArray(size);

        for (int i = 0; i < map.data.Length; i++)
        {
            int x = map.GetX(i);
            int y = map.GetY(i);

            SetMapTileValue(x, y, map);
        }

        return map;
    }

    private static void SetMapTileValue(int x, int y, OneDArray map)
    {
        switch (PositionHasObstacle(new Vector3(x, 0, y)))
        {
            case true:
                map.data[map.GetIndex(y, x)] = -1;
                break;
            case false:
                map.data[map.GetIndex(y, x)] = 0;
                break;
        }
    }

    private static bool PositionHasObstacle(Vector3 vector3)
    {
        vector3 -= Vector3.up;

        Ray ray = new Ray(vector3, Vector3.up);

        LayerMask layer = LayerMask.GetMask("AStarObstacle");

        int hits = Physics.RaycastNonAlloc(ray, RESULT, Mathf.Infinity, layer);

        return hits != 0;
    }

#endif
}
