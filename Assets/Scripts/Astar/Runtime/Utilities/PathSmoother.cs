﻿using UnityEngine;
using System.Collections.Generic;

// https://answers.unity.com/questions/392606/line-drawing-how-can-i-interpolate-between-points.html

public class PathSmoother
{
    public static List<Vector2> MakeSmoothCurve(List<Vector2> arrayToCurve, float iterations)
    {
        List<Vector2> points;
        List<Vector2> curvedPoints;

        if (iterations < 1.0f) iterations = 1.0f;

        int pointsLength = arrayToCurve.Count;

        int curvedLength = pointsLength * Mathf.RoundToInt(iterations) - 1;

        curvedPoints = new List<Vector2>(curvedLength);

        for (int pointInTimeOnCurve = 0; pointInTimeOnCurve < curvedLength + 1; pointInTimeOnCurve++)
        {
            float step = Mathf.InverseLerp(0, curvedLength, pointInTimeOnCurve);
            points = new List<Vector2>(arrayToCurve);

            for (int i = pointsLength - 1; i > 0; i--)
                for (int k = 0; k < i; k++)
                    points[k] = (1 - step) * points[k] + step * points[k + 1];

            curvedPoints.Add(points[0]);
        }

        return curvedPoints;
    }
}