﻿using DutchSkull.Astar;
using UnityEngine;

[ExecuteAlways]
public class PathFinderManagerEditor : MonoBehaviour
{
#if UNITY_EDITOR

    #region Editor Astar Initializing

    [SerializeField] private Vector2Int size = new Vector2Int(25, 25);
    private Vector2Int Size => size;

    private ScriptableObjectBuilder<AStarMapData> mapData;

    private void Start()
    {
        if (!enabled)
            return;

        if (Application.isPlaying)
            return;

        if (size.x <= 0 || size.y <= 0)
        {
            Log.Warning($"{nameof(PathFinderManagerEditor)}: The map size can't be negative or 0. Please check.", false);
            return;
        }

        mapData = new ScriptableObjectBuilder<AStarMapData>(AstarConfig.FolderPath, AstarConfig.AssetName);

        mapData.ScriptableObject.map = AStarMapGenerator.Generate(Size);
    }
    #endregion

    #region Gizmos

    private Vector3 nodeSize = new Vector3(1, 0, 1);

    private void OnDrawGizmos()
    {
        if (!enabled)
            return;

        if (mapData == null)
            return;

        DrawNodes(mapData.ScriptableObject.map);
    }

    private void DrawNodes(OneDArray map)
    {
        for (int i = 0; i < map.data.Length; i++)
        {
            int nodeValue = map.data[i];

            Gizmos.color = nodeValue == 0 ? Color.white : Color.red;

            int x = map.GetX(i);
            int y = map.GetY(i);

            Vector3 center = new Vector3(y, 0, x);

            Gizmos.DrawWireCube(center, nodeSize);
        }
    }
    #endregion

#endif
}
