﻿namespace DutchSkull.Astar
{
    public static class NodeExtensions
    {
        internal static Node[] Successors(this Node node, AStarMapData mapData, int rows, int cols, HeuristicMode heuristic)
        {
            SuccessorsData data = new SuccessorsData(mapData, rows, cols, node);

            if (heuristic == HeuristicMode.Diagonal || heuristic == HeuristicMode.Euclidean)
            {
                if (heuristic == HeuristicMode.Manhattan)
                    return DiagonalSuccessorsFree(data);
                else
                    return DiagonalSuccessors(data);
            }

            return data.result;
        }

        private static Node[] DiagonalSuccessors(SuccessorsData data)
        {
            if (data.xNorth)
            {
                if (data.xEast && data.grid[data.mapData.map.GetIndex(data.north, data.east)] == 0)
                    data.result[data.i++] = new Node(data.east, data.north);

                if (data.xWest && data.grid[data.mapData.map.GetIndex(data.north, data.west)] == 0)
                    data.result[data.i++] = new Node(data.west, data.north);
            }

            if (data.xSouth)
            {
                if (data.xEast && data.grid[data.mapData.map.GetIndex(data.south, data.east)] == 0)
                    data.result[data.i++] = new Node(data.east, data.south);

                if (data.xWest && data.grid[data.mapData.map.GetIndex(data.south, data.west)] == 0)
                    data.result[data.i++] = new Node(data.west, data.south);
            }

            return data.result;
        }

        private static Node[] DiagonalSuccessorsFree(SuccessorsData data)
        {
            data.xNorth = data.north > -1;
            data.xSouth = data.south < data.rows;
            data.xEast = data.east < data.cols;
            data.xWest = data.west > -1;

            if (data.xEast)
            {
                if (data.xNorth && data.grid[data.mapData.map.GetIndex(data.north, data.east)] == 0)
                    data.result[data.i++] = new Node(data.east, data.north);

                if (data.xSouth && data.grid[data.mapData.map.GetIndex(data.south, data.east)] == 0)
                    data.result[data.i++] = new Node(data.east, data.south);
            }
            if (data.xWest)
            {
                if (data.xNorth && data.grid[data.mapData.map.GetIndex(data.north, data.west)] == 0)
                    data.result[data.i++] = new Node(data.west, data.north);

                if (data.xSouth && data.grid[data.mapData.map.GetIndex(data.south, data.west)] == 0)
                    data.result[data.i++] = new Node(data.west, data.south);
            }
            return data.result;
        }
    }
}