using UnityEngine.SceneManagement;

namespace DutchSkull.Astar
{
    public static class AstarConfig
    {
        public const string MAP_DATA_IDENTIFIER = "MapData";
        public const string ROOT_FOLDER_PATH = "Assets";
        public const string RESOURCE_FOLDER_NAME = "Resources";

        public static string FolderPath => $"{ROOT_FOLDER_PATH}/{RESOURCE_FOLDER_NAME}";
        public static string AssetName => $"{SceneManager.GetActiveScene().name}_{MAP_DATA_IDENTIFIER}";
    }
}
