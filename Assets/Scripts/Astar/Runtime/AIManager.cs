using UnityEngine;
using DutchSkull.Singleton;
using System.Collections.Generic;
using System.Linq;

public class AIManager : SingleSceneSingleton<AIManager>
{
    private List<Transform> allAI = new List<Transform>();

    private Dictionary<int, Transform> closestAI = new Dictionary<int, Transform>();

    private int Count => allAI.Count;

    protected override void Awake()
    {
        SetInstance(this);

        // Getting all AI through AStarAgent
        allAI = (from ai in FindObjectsOfType<AstarAgent>()
                 select ai.transform).ToList();

        // Initializing the closest dictionary
        for (int i = 0; i < allAI.Count; i++)
            closestAI[allAI[i].GetInstanceID()] = null;

        UpdateClosest();
    }

    private void FixedUpdate() => UpdateClosest();

    //TODO Only update the AI's that have moved, is optimization if there is time 
    private void UpdateClosest()
    {
        for (int i = 0; i < Count; i++)
        {
            int index = GetClosestIndex(allAI[i]);

            int instanceId = allAI[i].GetInstanceID();

            if (closestAI[instanceId] == allAI[index])
                continue;

            closestAI[instanceId] = allAI[index];
        }
    }

    private int GetClosestIndex(Transform currentAI)
    {
        float distance = float.MaxValue;
        int index = 0;

        for (int k = 0; k < Count; k++)
        {
            if (currentAI == allAI[k])
                continue;

            float currentDistance = Vector3.Distance(currentAI.position, allAI[k].position);

            if (distance < currentDistance)
                continue;

            distance = currentDistance;
            index = k;
        }

        return index;
    }

    public Transform Closest(Transform transform) => closestAI[transform.GetInstanceID()];
}
