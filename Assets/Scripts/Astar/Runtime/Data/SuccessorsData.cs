﻿namespace DutchSkull.Astar
{
    public struct SuccessorsData
    {
        public bool xNorth;
        public bool xSouth;
        public bool xEast;
        public bool xWest;

        public int north;
        public int south;
        public int east;
        public int west;

        public AStarMapData mapData;
        public int[] grid;

        public int rows;
        public int cols;

        public Node[] result;

        public int i;

        public SuccessorsData(AStarMapData mapData, int rows, int cols, Node node)
        {
            this.mapData = mapData;

            this.grid = mapData.map.data;
            this.rows = rows;
            this.cols = cols;

            north = node.Y - 1;
            south = node.Y + 1;
            east = node.X + 1;
            west = node.X - 1;

            xNorth = north > -1 && this.grid[mapData.map.GetIndex(north, node.X)] == 0;
            xSouth = south < this.rows && this.grid[mapData.map.GetIndex(south, node.X)] == 0;
            xEast = east < this.cols && this.grid[mapData.map.GetIndex(node.Y, east)] == 0;
            xWest = west > -1 && this.grid[mapData.map.GetIndex(node.Y, west)] == 0;

            i = 0;

            result = new Node[8];

            if (xNorth)
                result[i++] = new Node(node.X, north);

            if (xEast)
                result[i++] = new Node(east, node.Y);

            if (xSouth)
                result[i++] = new Node(node.X, south);

            if (xWest)
                result[i++] = new Node(west, node.Y);
        }
    }
}