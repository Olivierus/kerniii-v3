﻿using UnityEngine;

public class AStarMapData : ScriptableObject
{
    [SerializeField] public OneDArray map;
}
