﻿using DutchSkull.Utilities;
using DutchSkull.Gizmos;
using System.Collections.Generic;
using UnityEngine;
using DutchSkull.Astar;

#pragma warning disable IDE0051, IDE0044

public class AstarAgent : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 1f;
    [SerializeField] private float rotationSpeed = 1f;
    [SerializeField] private MoveMethod moveMethod = MoveMethod.CharacterController;

    public float RemainingDistance => Vector2.Distance(transform.PostitionXZ(), destination);

    public bool isMoving = false;

    private Vector2 destination = default;
    private Vector2 start = default;

    private FollowPath followPath = null;

    private void Start()
    {
        destination = new Vector2(-1, -1);
    }

    private void FixedUpdate()
    {
        if (followPath == null)
            return;

        followPath.MoveOnPath();
    }

    public bool SetDestination(Vector2 destination)
    {
        isMoving = true;

        followPath = null;

        Vector2 startPosition = transform.PostitionXZ();

        bool hasFoundAPath = Astar.GetPath(startPosition, destination, out List<Vector2> path);

        if (!hasFoundAPath)
            return false;

        this.destination = destination;

        start = startPosition;

        followPath = new FollowPath(path, transform, moveSpeed, rotationSpeed, moveMethod);

        return true;
    }

    public void Stop()
    {
        destination = new Vector2(-1, -1);
        isMoving = false;
        followPath = null;
    }

    private void OnDrawGizmos()
    {
        if (!enabled || !Application.isPlaying)
            return;

        GizmosExtension.DrawBeginAndEndPoint(start, destination);

        if (followPath == null || followPath.Path == null)
            return;

        GizmosExtension.DrawPath(followPath.Path);
    }

    private Astar Astar => Astar.Instance;
}
