﻿using UnityEngine;

#pragma warning disable IDE0051

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float normalSpeed = 1f;
    [SerializeField] private float extraSpeed = 1f;

    private Mover mover = default;

    private void Awake() => mover = new Mover(transform, normalSpeed, MoveMethod.CharacterController);

    private void Update()
    {
        Vector3 inputPosition = transform.position + new Vector3(MoveForward, 0f, Strafe);

        mover.Move(inputPosition);

        if (Input.GetKeyDown(KeyCode.LeftShift))
            mover.moveSpeed += extraSpeed;
        else if (Input.GetKeyUp(KeyCode.LeftShift))
            mover.moveSpeed -= extraSpeed;
    }

    private float Strafe => Input.GetAxis("Vertical");

    private float MoveForward => Input.GetAxis("Horizontal");
}
