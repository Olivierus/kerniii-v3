﻿using System;
using UnityEngine;

/// <summary>
/// Contains the lines and audio for a dialogue line
/// </summary>
[Serializable]
public class DialogueLine
{
    public string speaker;
    public string line;
    public AudioClip audio;
}
