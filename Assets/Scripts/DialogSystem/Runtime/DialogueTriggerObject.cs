﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class DialogueTriggerObject : MonoBehaviour
{
    public bool triggerByTouch;

    [SerializeField] private DialogueObject dialogue = default;
    [SerializeField] private UnityEvent preDialogueMethod = default;
    [SerializeField] private UnityEvent dialogueMethod = default;

    public bool Interacted { get; set; }

    public void Interact()
    {
        Interacted = true;
        dialogueMethod.AddListener(() => { Interacted = false; Done(); });

        TriggerDialogue();

        if (preDialogueMethod != null)
            preDialogueMethod.Invoke();

        Done();
    }

    private void Start()
    {
        gameObject.layer = LayerMask.NameToLayer("Interactable");
        GetComponent<Collider>().isTrigger = triggerByTouch;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!triggerByTouch)
            return;

        if (other.CompareTag("Player"))
            Interact();
    }

    private void TriggerDialogue()
    {
        if (dialogue == null)
        {
            Debug.LogError(gameObject.name + " has no dialogue object.");
            return;
        }

        if (dialogue.lines.Length < 1)
        {
            Debug.LogError(gameObject.name + " has no dialogue lines.");
            return;
        }

        TMP_Text tmpText = gameObject.GetComponentInChildren<TMP_Text>();

        if (tmpText)
            DialogueManager.Instance.StartDialogue(dialogue, gameObject, dialogueMethod);
        else
            DialogueManager.Instance.StartDialogue(dialogue, null, dialogueMethod);
    }

    [ExecuteInEditMode]
    private void OnDrawGizmos()
    {
        if (triggerByTouch)
        {
            Gizmos.color = new Color(0, 0, 0.5f, 0.25f);
            Gizmos.DrawCube(transform.position, transform.localScale);
        }

        Gizmos.DrawIcon(transform.position, "DialogueGizmo.png", true);
    }

    public void Done() => enabled = false;
}
